#!/bin/bash
find . -name "*.toc"  -print -delete
find . -name "*.snm"  -print -delete
find . -name "*.out"  -print -delete
find . -name "*.nav"  -print -delete
find . -name "*.aux"  -print -delete
find . -name "*.tex~"  -print -delete
find . -name "*.tex.backup"  -print -delete
find . -name "*.blg"  -print -delete
find . -name "*.bbl"  -print -delete
find . -name "*.synctex.gz"  -print -delete
find . -name "*.mtc*"  -print -delete
find . -name "*.maf"  -print -delete
find . -name "*.lof"  -print -delete
find . -name "*.log"  -print -delete
find . -name "*.glo"  -print -delete
find . -name "*.ist"  -print -delete
find . -name "*.run.xml"  -print -delete
find . -name "*.bcf"  -print -delete
#find . -name "*.pdf"  -print -delete

